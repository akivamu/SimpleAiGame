PFont font;

int WORLD_WIDTH = 800;
int WORLD_HEIGHT = 800;
PVector GOAL_POS = new PVector(WORLD_WIDTH/2, 10);
PVector SUBJECT_INIT_POS = new PVector(WORLD_WIDTH/2, WORLD_HEIGHT - 10);
int SUBJECT_MAX_MOVES = 1000;
float SUBJECT_FREEDOM_RATE = 0.1;
int GENERATION_SIZE = 1000;

int OBS_W = WORLD_WIDTH / 2;
int OBS_H = WORLD_HEIGHT / 10;
int OBS_X = (WORLD_WIDTH - OBS_W) / 2;
int OBS_Y = WORLD_HEIGHT / 2;

Generation generation;
ArrayList<Obstacle> obstacles = new ArrayList<Obstacle>();

void setup() {
  size(800, 800); //size of the window
  frameRate(1000);//increase this to make the dots go faster
  font = createFont("Arial", 16, true);
  generation = new Generation(0, GENERATION_SIZE, null);
  
  obstacles.add(new Obstacle(0, 400, 600, 50));
  obstacles.add(new Obstacle(200, 600, 600, 50));
}

boolean isCollide(PVector pos) {
  if (pos.x< 2|| pos.y<2 || pos.x>WORLD_WIDTH-2 || pos.y>WORLD_HEIGHT -2) { // collide with edge 
    return true;
  }

  for (Obstacle obstacle : obstacles) {
    if (obstacle.isCollide(pos)) {
      return true;
    }
  }
  return false;
}

void draw() { 
  background(255);

  // Draw goal
  fill(255, 0, 0);
  ellipse(GOAL_POS.x, GOAL_POS.y, 10, 10);

  //draw obstacle(s)
  for (Obstacle obstacle : obstacles) {
    obstacle.render(#0000ff);
  }

  // Process and show
  if (generation.isAllDone()) {
    // Find best subject in generation
    Subject bestSubject = generation.findBestSubject();

    // Create new generation base on best brain
    generation = new Generation(generation.id + 1, GENERATION_SIZE, bestSubject);
  } else {
    generation.update();
    generation.renderAllSubjects();
  }


  // Draw info
  textFont(font, 16);
  fill(0);        
  text("Gen #"+ generation.id + ", shortest moves: " + generation.getShortestMoves(), 10, 50);
}

class Obstacle {
  int x; int y; int w; int h;
  public Obstacle(int x, int y, int w, int h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  
  boolean isCollide(PVector pos) {
    return (x < pos.x && pos.x < x + w && y < pos.y && pos.y < y + h);
  }
  
  void render(int colr) {
    fill(colr);
    rect(x, y, w, h);
  }
}
