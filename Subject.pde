class Subject {
  ArrayList<PVector> refMemory; // reference memory from parent
  
  float freedomRate; // chance to make its own decision
  ArrayList<PVector> memory = new ArrayList<PVector>(); // current memory
  float rating = 0;

  PVector pos;
  private PVector vel = new PVector(0,0);
  
  // Set by others
  boolean isActive = true;
  boolean isReachedGoal = false;

  Subject(PVector initPos, float freedomRate, ArrayList<PVector> refMemory) {
    this.pos = initPos.copy();
    this.freedomRate = freedomRate;
    
    this.refMemory = refMemory;
  }

  private PVector makeMyOwnDecision() {
    // Still stupid, just a random move
    float randomAngle = random(2*PI);
    return PVector.fromAngle(randomAngle); 
  }

  void decideAndMove() {
    int index = memory.size(); // next decision index
    boolean shouldMakeMyOwnDecision = random(1) < freedomRate;

    // If no parent memory, always make decide on my own
    if (refMemory == null || index >= refMemory.size()) {
      shouldMakeMyOwnDecision = true;
    }

    PVector nextMoveDirection = shouldMakeMyOwnDecision
                                ? makeMyOwnDecision()
                                : refMemory.get(index);
    
    // Remember
    memory.add(nextMoveDirection);
    
    // Move
    vel.add(nextMoveDirection);
    vel.limit(5);
    pos.add(vel);
  }
  
  float calculateDistanceTo(PVector target) {
    return dist(pos.x, pos.y, target.x, target.y);
  }
}
