
class Generation {
  int id;
  private Subject[] subjects;
  private Subject refSubject;
  int bestMoves = SUBJECT_MAX_MOVES;

  Generation(int id, int size, Subject refSubject) {
    this.id = id;

    ArrayList<PVector> refMemory = null;
    if (refSubject != null) {
      refMemory = refSubject.memory;
      bestMoves = refMemory.size();

      // Recreate refSubject for drawing purpose. Always follow parent, no freedom
      this.refSubject = new Subject(SUBJECT_INIT_POS, 0, refMemory);
    }

    subjects = new Subject[size];
    for (int i = 0; i< size; i++) {
      subjects[i] = new Subject(SUBJECT_INIT_POS, SUBJECT_FREEDOM_RATE, refMemory);
    }
  }

  int getShortestMoves() {
    return bestMoves;
  }

  void update() {
    // Check all subjects
    for (Subject subject : subjects) {
      updateSubject(subject);
    }

    // Replay refSubject
    if (refSubject != null) {
      updateSubject(refSubject);
    }
  }

  private void updateSubject(Subject subject) {
    // Check state
    if (subject.isActive) {

      // Reach max move allowed
      if (subject.memory.size() >= SUBJECT_MAX_MOVES) {
        subject.isActive = false;
      }

      // Or surpass shortest moves of generation
      //if (subject.memory.size() > 1.1 * getShortestMoves()) {
      //  subject.isActive = false;
      //}

      // Collide
      if (isCollide(subject.pos)) {
        subject.isActive = false;
      }

      // Check if reach goal
      if (subject.calculateDistanceTo(GOAL_POS) < 5) {
        subject.isReachedGoal = true;
        subject.isActive = false;
      }
    }

    // Make decision and move if active
    if (subject.isActive) {
      subject.decideAndMove();
    }
  }

  void renderAllSubjects() {
    fill(0);
    for (Subject subject : subjects) {
      ellipse(subject.pos.x, subject.pos.y, 4, 4);
    }

    if (refSubject != null) {
      fill(0, 255, 0);
      ellipse(refSubject.pos.x, refSubject.pos.y, 8, 8);
    }
  }

  Subject findBestSubject() {
    float bestRating = 0;
    Subject bestSubject = subjects[0];
    for (Subject subject : subjects) {
      float rating = calculateSubjectRating(subject);
      if (rating > bestRating) {
        bestSubject = subject;
        bestRating = rating;
      }
    }
    if (refSubject != null) {
      if (refSubject.rating > bestSubject.rating) {
        bestSubject = refSubject;
      }
    }
    return bestSubject;
  }

  boolean isAllDone() {
    for (Subject subject : subjects) {
      if (subject.isActive) {
        return false;
      }
    }
    return true;
  }

  private float calculateSubjectRating(Subject subject) {
    float rating = 0;
    if (subject.isReachedGoal) { //if the dot reached the goal then the fitness is based on the amount of steps it took to get there
      rating = 1.0/16.0 + 10000.0/(float)(subject.memory.size() * subject.memory.size());
    } else {//if the dot didn't reach the goal then the fitness is based on how close it is to the goal
      float distanceToGoal = subject.calculateDistanceTo(GOAL_POS);
      rating = 1.0/(distanceToGoal * distanceToGoal);
    }

    return rating;
  }
}
